git_repository_name = "ping-cloud-update-config"
custom_tags = {
  DeploymentType = "Terraform"
  Environment    = "Deployment"
  TargetAccounts = "Demo"
}
code_pipeline_build_stages = {
  build        = "buildspec/build.yaml"
  pingaccess   = "buildspec/pingaccess.yaml"
  pingfederate = "buildspec/pingfederate.yaml"
}
codebuild_image          = "aws/codebuild/amazonlinux2-x86_64-standard:3.0"
branches                 = ["dev"]
codebuild_log_group_name = "/aws/codebuild/ping-cloud-update-config"
